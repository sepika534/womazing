$(function () {
    // Слайдер1

    $('.slider').slick({
        infinite: true,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        arrows: false,

    });

    // Слайдер2

    $('.swiper').slick({
        infinite: true,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    slidesToScroll: 1
                }
            }
        ]
    });

    // Модальное окно

    $('.phone-btn').on('click', function () {
        $('.wrapper-modal').fadeIn();
    });
    $('.select-btn').on('click', function () {
        $('.wrapper-modal__call').fadeIn();
    });
    $('.form-book').on('click', function () {
        $('.wrapper-modal').fadeOut();
    });
    $('.overlay').on('click', function () {
        $('.wrapper-modal').fadeOut();
    });
    $('.overlay').on('click', function () {
        $('.wrapper-modal__call').fadeOut();
    });
    $('.modal-window_close').on('click', function () {
        $('.wrapper-modal').fadeOut();
    });
    $('.call-btn').on('click', function () {
        $('.wrapper-modal__call').fadeOut();
    });
    $('.form-book').children().on('click', function (e) {
        e.stopPropagation();
    });


    // Фиксированная шапка

    $(document).ready(function () {
        const headerOffset =
            $("#head").offset().top;

        $(window).scroll(function () {
            const scrolled =
                $(this).scrollTop();

            if (scrolled > 700) {
                $("#header").addClass("header-fixed");
            } else if (scrolled < headerOffset) {
                $("#header").removeClass("header-fixed")
            }
        });
    });






    //Валидация и отправка формы
    $(document).ready(function () {
        $('.select-btn').on('click', function (e) {
            e.preventDefault();
            $(this).parent('form').submit();
        })
        $.validator.addMethod(
            "regex",
            function (value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
        );

        // Функция валидации и вывода сообщений
        function valEl(el) {
            el.validate({
                rules: {
                    phoneNumberBook: {
                        required: true,
                        digits: true,
                        minlength: 10,
                        maxlength: 13,
                        regex: "[0-9]+"
                    },
                    firstNameBook: {
                        required: true
                    },
                    emailNameBook: {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    phoneNumberBook: {
                        required: 'Поле обязательно для заполнения',
                        regex: 'Телефон может содержать символы + - ()'
                    },
                    firstNameBook: {
                        required: 'Поле обязательно для заполнения',
                    },
                    emailNameBook: {
                        required: 'Поле обязательно для заполнения',
                        email: 'Неверный формат E-mail'
                    }
                },

                // Начинаем проверку id="" формы
                submitHandler: function (form) {
                    $('#loader').fadeIn();
                    var $form = $(form);
                    var $formId = $(form).attr('id');
                    switch ($formId) {

                        // Если у формы id="popupResult" - делаем:
                        case 'form-book':
                            $.ajax({
                                type: 'POST',
                                url: $form.attr('action'),
                                data: $form.serialize()
                            })
                                .done(function () {
                                    console.log('Success');
                                })
                                .fail(function () {
                                    console.log('Fail');
                                    setTimeout(function () {

                                        //строки для остлеживания целей в Я.Метрике и Google Analytics
                                    }, 1100);

                                })
                                .always(function () {
                                    console.log('Always');
                                    setTimeout(function () {

                                        $form.trigger('reset');
                                        //строки для остлеживания целей в Я.Метрике и Google Analytics
                                    }, 1100);


                                });
                            break;
                    }
                    return false;
                }
            })
        }

        // Запускаем механизм валидации форм, если у них есть класс .js-form
        $('.form-book').each(function () {
            valEl($(this));
        });

    });





    //Валидация и отправка формы
    $(document).ready(function () {
        $('.write-btn').on('click', function (e) {
            e.preventDefault();
            $(this).parent('form').submit();
        })
        $.validator.addMethod(
            "regex",
            function (value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
        );

        // Функция валидации и вывода сообщений
        function valEl(el) {
            el.validate({
                rules: {
                    phoneNumberWrite: {
                        required: true,
                        digits: true,
                        minlength: 10,
                        maxlength: 13,
                        regex: "[0-9]+"
                    },
                    firstNameWrite: {
                        required: true
                    },
                    emailNameWrite: {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    phoneNumberWrite: {
                        required: 'Поле обязательно для заполнения',
                        regex: 'Телефон может содержать символы + - ()'
                    },
                    firstNameWrite: {
                        required: 'Поле обязательно для заполнения',
                    },
                    emailNameWrite: {
                        required: 'Поле обязательно для заполнения',
                        email: 'Неверный формат E-mail'
                    }
                },

                // Начинаем проверку id="" формы
                submitHandler: function (form) {
                    $('#loader').fadeIn();
                    var $form = $(form);
                    var $formId = $(form).attr('id');
                    switch ($formId) {

                        // Если у формы id="popupResult" - делаем:
                        case 'write-form':
                            $.ajax({
                                type: 'POST',
                                url: $form.attr('action'),
                                data: $form.serialize()
                            })
                                .done(function () {
                                    console.log('Success');
                                })
                                .fail(function () {
                                    console.log('Fail');
                                    setTimeout(function () {
                                        $('#write-luck__fail').fadeIn();

                                        //строки для остлеживания целей в Я.Метрике и Google Analytics
                                    }, 1100);

                                })
                                .always(function () {
                                    console.log('Always');
                                    setTimeout(function () {
                                        $('#write-luck').fadeIn();
                                        $form.trigger('reset');
                                        //строки для остлеживания целей в Я.Метрике и Google Analytics
                                    }, 1100);
                                    $('#write-luck').on('click', function (e) {
                                        $(this).fadeOut();
                                    });

                                });
                            break;
                    }
                    return false;
                }
            })
        }

        // Запускаем механизм валидации форм, если у них есть класс .js-form
        $('.write-form').each(function () {
            valEl($(this));
        });

    });




    // Hamburger

    $('.hamburger').on('click', function () {
        $('.head__menu').toggle();
    });




    $('.sorting-btn').on('click', function () {
        $('.sorting-container').toggle();
    });

    $('.sorting-item__btn ').on('click', function () {
        var currTab = $(this).parent().index();

        $('.sorting-item__btn').removeClass('active');
        $(this).addClass('active');

        $('.sorting-item__btn').removeClass('active');
        $('.sorting-item__btn').eq(currTab).addClass('active');;
    });








    // Создание карты.
    ymaps.ready(function () {

        myMap = new ymaps.Map("map", {

            center: [55.684948056920014, 37.53747575416718],
            zoom: 17,
            controls: []
        }),

            myPlacemark = new ymaps.Placemark(myMap.getCenter(), {},
                {
                    iconLayout: 'default#image',
                    iconImageHref: 'img/gps.png',
                    iconImageSize: [50, 50],
                    iconImageOffset: [-40, -60],
                });

        myMap.geoObjects
            .add(myPlacemark);
    });

});